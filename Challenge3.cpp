#include <iostream>
#include <string>
#include <cstring>
using namespace std;

bool pal(string str)
{
bool valid;
int i;
for (i = 0; i < str.length(); i++)
{
    if (str[-i] == str[i])
    {
        valid = true;
    }
    else
    {
        valid = false;
    }
}
return valid;
}

int main()
{
string s;
cin >> s;

if (!pal(s))
{
    cout << "NO" << endl;
}
else
{
    cout << "YES" << endl;
}
return 0;
    }
